class Animal {
    constructor(name) {
        this.kucing = name;
        this.daging = 4;
        this.indonesia = false;
    } get name() {
        return this.kucing;
    } get legs() {
        return this.daging;
    }
}
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.kucing = name;
        this.daging = 2;
    }
    yell() { return console.log("Auooo"); }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.kucing = name;
    }
    jump() { return console.log("hop hop"); }
}

let sheep = new Animal("shaun");
console.log(sheep.kucing) // "shaun"
console.log(sheep.daging) // 4
console.log(sheep.indonesia) // false
console.log("");

let sungokong = new Ape("kera sakti")
console.log(sungokong.kucing); //"kera sakti"
console.log(sungokong.daging); //2
console.log(sungokong.indonesia) // false
sungokong.yell() // "Auooo"
console.log("");

let kodok = new Frog("buduk")
console.log(kodok.kucing); //buduk
console.log(kodok.daging); //4
console.log(kodok.indonesia) // false
kodok.jump() // "hop hop"
console.log("");

//02.
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();