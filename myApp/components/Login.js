import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ImageBackground, Image, SafeAreaView, TextInput, onChangeText } from 'react-native';

export default function LOGIN() {
    const [username, onChangeText] = React.useState(null);
    const [pass, onChangePass] = React.useState(null);
    return (
        <View style={styles.canvas} >
            <View style={styles.container}>

                <Image style={styles.header2} source={require('../assets/header.png')} />
                <Image style={styles.header2} source={require('../assets/header2.png')} />
                <Image style={styles.footer} source={require('../assets/ok1.jpg')} />
                <Image style={styles.logo} source={require('../assets/ok2.jpg')} />

            </View>
            <View style={styles.form}>
                <Text style={styles.textLogin}>Gmail</Text>
                <SafeAreaView>
                    <TextInput style={styles.input} onChangeText={onChangeText} value={username}>
                    </TextInput>
                </SafeAreaView>
                <Text style={styles.textLogin}>Password</Text>
                <SafeAreaView>
                    <TextInput style={styles.input} onChangeText={onChangePass} value={pass}>
                    </TextInput>
                </SafeAreaView>
                <TouchableOpacity style={styles.btnSignin}
                    onPress={() => document.location.href = 'Login.js'}>
                    <Text style={styles.signin}>LOG IN</Text>
                </TouchableOpacity>
            </View>


        </View >
    )
};

const styles = StyleSheet.create({
    canvas: {
        height: 640,
        backgroundColor: 'black',
        alignItems: 'center',

    },
    container: {
        width: 360,
        height: 640,
        backgroundColor: '#379BD3',
        marginBottom: -500,

    },
    top: {
        width: 360,
        height: 150,

    },
    title: {
        position: 'absolute',
        top: 200,
        left: 80,
        fontWeight: 'bold',
        fontSize: 36,
        fontFamily: 'Ribeye'
    },
    btnSignin: {
        position: 'absolute',
        top: 400,
        backgroundColor: 'black',
        height: 37,
        alignItems: 'center',
        justifyContent: 'center',
        width: 123,
    },
    signin: {
        fontFamily: 'sans-serif',
        color: 'white'
    },
    kotak: {
        width: 307,
        height: 196,
        backgroundColor: '#379BD3',
        position: 'absolute',
        top: 440
    },
    footer: {
        width: 360,
        height: 75,
    },
    footer2: {
        width: 360,
        height: 75,
    },
    header2: {
        position: 'absolute',
        top: 0,
        width: 360,
        height: 200,
    },
    logo: {
        width: 150,
        height: 150,
        borderRadius: 100,
        position: 'absolute',
        top: 40,
        left: 100,
    },
    btnSignin: {
        position: 'absolute',
        left: 75,
        top: 200,
        backgroundColor: 'black',
        height: 37,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        width: 123,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black'
    },
    signin: {
        fontFamily: 'sans-serif',
    },
    form: {
        marginTop: 100,
        width: 275,
        height: 100,
    },
    input: {
        width: 275,
        height: 30,
        borderWidth: 1,
        marginBottom: 30,
        paddingLeft: 15,
        backgroundColor: 'white'
    }
})