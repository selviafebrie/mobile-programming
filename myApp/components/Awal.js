import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ImageBackground, Image } from 'react-native';

export default function Awal() {
    return (
        <View style={styles.canvas} >
            <View style={styles.container}>
                <Image style={styles.top} source={require('../assets/header.png')} />
                <Text style={styles.title}>DATA USER</Text>
            </View>
            <TouchableOpacity style={styles.btnSignin}
                onPress={() => document.location.href = 'Login.js'}>
                <Text style={styles.signin}>LOGIN</Text>
            </TouchableOpacity>
            <View style={styles.kotak}></View>
            <Image style={styles.footer} source={require('../assets/ok1.jpg')} />
        </View >
    )
};

const styles = StyleSheet.create({
    canvas: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',

    },
    container: {
        width: 360,
        height: 640,
        backgroundColor: '#0F4E89',

    },
    top: {
        width: 360,
        height: 160,

    },
    title: {
        position: 'absolute',
        top: 200,
        left: 80,
        fontWeight: 'bold',
        fontSize: 36,
        fontFamily: 'Ribeye'
    },
    btnSignin: {
        position: 'absolute',
        top: 400,
        backgroundColor: 'black',
        height: 37,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        width: 123,
    },
    signin: {
        fontFamily: 'sans-serif',
        color: 'white'
    },
    kotak: {
        width: 307,
        height: 196,
        backgroundColor: '#379BD3',
        position: 'absolute',
        top: 440
    },
    footer: {
        position: 'absolute',
        top: 480,
        width: 360,
        height: 160,
    }
})